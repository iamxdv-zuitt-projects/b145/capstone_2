//[SECTION] Dependencies and Modules
const express = require('express'); //express module
const { Mongoose } = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/userRoutes')
const adminRoutes = require('./routes/adminRoutes')
const paMineRoutes = require('./routes/paMineRoutes');
const dotenv = require('dotenv');

//[SECTION] Environment Variables Setup
    //Configure the application in order for it to recognize and identify the necessary components need to build the app successfully
    dotenv.config();
    //extract the variables from the .env file
    // verify the variable by displaying its value in the console. make sure to identify the origin of the component
    const secret = process.env.CONNECTION_STRING


// [SECTION] Server Setup
const port = process.env.PORT || 3000; // any port as long as free
const app = express(); //create server thru variable 'app' thru module express.

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

//[SECTION] Server Routes
app.use('/users', userRoutes)
app.use('/admin', adminRoutes)
app.use('/paMine', paMineRoutes)


// [SECTION] Server Response
app.get('/', (req,res) => {
    res.send('hosted in heroku')
})
app.listen(port, () => console.log(`Server is running at port ${port}`))

//[SECTION] DATABASE CONNECT

    //Connecting MongoDB
        
        const mongoose = require('mongoose'); // For connecting MongoDB
        //Connection String
        mongoose.connect(secret, 
                {
                    useNewUrlParser: true,
                    useUnifiedTopology: true
                }
        );

        // Connection status from Server
        let db = mongoose.connection;

        db.on("error", console.error.bind(console, "There is an error with the connection"));
        db.once("open", () => console.log("Successfully connected to the MongoDB"));
