const express = require('express');
const router = express.Router();
const userController = require('../controller/userControllers')
const auth = require('../auth')


/*====================[SECTION] REGISTRATION==================== */


router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(fromController => res.send(fromController))
});
/*====================[END] REGISTRATION==================== */




/*====================[SECTION] RETRIEVE ALL USERS==================== */
router.get('/all-users', auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization)

	userController.allUsers(data).then(fromAdminController => res.send(fromAdminController))
});
/*======================[END] RETRIEVE ALL USERS====================== */





/*====================[SECTION] USER AUTHENTICATION==================== */
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(fromController => res.send(fromController))
});
/*====================[END] USER AUTHENTICATION==================== */





/*====================[SECTION] SET ADMIN FUNCTIONALITY==================== */
router.put('/:userId', auth.verify, (req, res) => {

	const data = {
		userId : req.params.userId,
		payload : auth.decode(req.headers.authorization)
	}

	userController.setToAdmin(data).then(fromController => res.send(fromController))
});
/*======================[END] SET ADMIN FUNCTIONALITY====================== */





/*====================[SECTION] CREATE ORDER (NON-ADMIN)==================== */
router.post("/order", auth.verify, (req, res) => {

	let data = {
		userId : auth.decode(req.headers.authorization).id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		itemId : req.body.itemId
	}

	userController.createOrder(data).then(fromController => res.send(fromController))
})
/*======================[END] CREATE ORDER (NON-ADMIN)====================== */




module.exports = router;