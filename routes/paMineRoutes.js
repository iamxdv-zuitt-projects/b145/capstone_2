const express = require('express');
const router = express.Router();
const paMineController = require('../controller/paMineControllers');
const auth = require('../auth')



/*============================[SECTION] ADD ITEM============================*/
router.post('/', auth.verify, (req, res) => {

	const data = {
		item : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	paMineController.addItem(data).then(fromAdminController => res.send(fromAdminController))
});

/*=============================[END] ADD ITEM=============================*/




/*======================[SECTION] RETRIEVE ALL ITEMS====================== */

router.get('/all-items', auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization)

	paMineController.allItems(data).then(fromController => res.send(fromController))
});

/*======================[END] SET RETRIEVE ALL ITEMS====================== */





/*====================[SECTION] RETRIEVE SPECIFIC ITEMS==================== */
router.get('/:itemId', (req, res) => {
	paMineController.getItem(req.params).then(fromController => res.send(fromController))
});
/*======================[END] RETRIEVE SPECIFIC ITEMS====================== */





/*=========================[SECTION] UPDATING ITEM========================= */
router.put('/update-item/:itemId', auth.verify, (req, res) => {

	const data = {
		itemId : req.params.itemId,
		payload : auth.decode(req.headers.authorization),
		updatedItem : req.body
	}

	paMineController.updateItem(data).then(fromController => res.send(fromController))
});
/*===========================[END] UPDATING ITEM=========================== */





/*=========================[SECTION] AVAILABLE ITEM========================= */
router.get('/', (req, res) => {
	paMineController.allAvailable().then(fromController => res.send(fromController))
});
/*==========================[END] AVAILABLE ITEM========================== */





/*=========================[SECTION] ARCHIVE ITEM======================== */
router.put('/archive/:itemId', auth.verify, (req, res) => {

	const data = {
		itemId : req.params.itemId,
		payload : auth.decode(req.headers.authorization)
	}

	paMineController.archiveItem(data).then(fromController => res.send(fromController))
});

/*=========================[END] ARCHIVE ITEM============================ */


module.exports = router;