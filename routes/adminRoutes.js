const express = require('express');
const router = express.Router();
const adminController = require('../controller/adminControllers')
const auth = require('../auth')





/*====================[SECTION] ADMIN REGISTRATION==================== */


router.post('/admin-register', (req, res) => {
	adminController.registerAdmin(req.body).then(fromAdminController => res.send(fromAdminController))
});
/*=======================[END] ADMIN REGISTRATION======================= */



/*====================[SECTION] ADMIN AUTHENTICATION==================== */
router.post('/admin-login', (req, res) => {
	adminController.loginAdmin(req.body).then(fromAdminController => res.send(fromAdminController))
});
/*======================[END] ADMIN AUTHENTICATION====================== */


/*====================[SECTION] RETRIEVE ALL ADMIN==================== */
router.get('/all-admin', auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization)

	adminController.allAdmin(data).then(fromAdminController => res.send(fromAdminController))
});
/*======================[END] RETRIEVE ALL ADMIN====================== */


module.exports = router;