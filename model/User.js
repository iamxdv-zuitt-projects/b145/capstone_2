const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

	firstName : {
		type: String,
		required: [true, "First name is required."]
	},
	lastName : {
		type: String,
		required: [true, "Last Name is required."]
	},
	gender : {
		type: String,
		required: [true, "Gender is required."]
	},
	email : {
		type: String,
		required: [true, "Email is required."]
	},
    password : {
		type: String,
		required: [true, "Password is required."]
    },
    isAdmin : {
		type: Boolean,
		default: false
	},
	isActive : {
		type: Boolean,
		default: true
	},
	mobileNo : {
		type: String,
		required: [true, "Mobile Number is required."]
    },
    products: [
        {
			itemId: {
                type: String,
                required: [true, "Item Id is required"]
            },
            purchasedOn: {
                type: Date,
                default: new Date()
            },
			status : {
				type: String,
				default: "Paid"
			}
        }
    ]
});

module.exports = mongoose.model('User', userSchema);