const mongoose = require('mongoose');

const damitSchema = new mongoose.Schema({

	name : {
		type: String,
		required: [true, "Item name is required"]
	},

	description : {
		type : String,
		required : [true, "Item description is required"]
	},
	size : {
		type: String,
		required : [true, "Item size is required"]
	},
	color : {
		type: String,
		required : [true, "Item color is required"]
	},

	price : {
		type: Number,
		required : [true, "Item price is required"]
	},

	isAvailable : {
		type : Boolean,
		default : true
	},

	createdOn : {
		type : Date,
		default : new Date()
	},
		
	mgaPaMine : [
		{
			userId : {
				type : String,
				required : [true, "UserId is required"]
			},

			purchasedOn: {
				type: Date,
				default: new Date()
			},
			totalAmount : {
				type : Number
				
			},
		}

	]
})

module.exports = mongoose.model('Item', damitSchema);
