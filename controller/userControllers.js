const User = require('../model/User');
const bcrypt = require('bcrypt');
const auth = require('../auth')
const paMine = require('../model/PaMine');


/*====================[SECTION] REGISTRATION CONTROLLER==================== */
module.exports.registerUser = (reqBody) => {

    let newUser = new User({
        firstName : reqBody.firstName,
        lastName : reqBody.lastName,
        gender : reqBody.gender,
        email : reqBody.email,
        mobileNo : reqBody.mobileNo,
        password : bcrypt.hashSync(reqBody.password, 10)
    })

    return newUser.save().then((user, err) => {

        if(err){

             return `Registration not successful`

        } else {

            return `Registration successful`
        }
    })
}
/*====================[END] REGISTRATION CONTROLLER==================== */




/*====================[SECTION] RETRIEVE ALL USERS==================== */
module.exports.allUsers = async (user) => {
	
    if(user.isAdmin === true){

        return User.find({}).then(result => {

            return result
        })

    } else {

        return `${user.email} is not ADMIN`
    }
}


/*======================[END] RETRIEVE ALL USERS====================== */



/*====================[SECTION] USER AUTHENTICATION==================== */
module.exports.loginUser = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {

		if(result == null){

			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password) 

			if(isPasswordCorrect){

				return { access: auth.createAccessToken(result)}
			} else {

				return false
			}
		}
	})
}
/*======================[END] USER AUTHENTICATION====================== */





/*====================[SECTION] SET ADMIN FUNCTIONALITY==================== */
module.exports.setToAdmin = async (data) => {

	if(data.payload.isAdmin === false) {

		return User.findById(data.userId).then((result, err) => {

			result.isAdmin = true;

			return result.save().then((userToAdmin, err) => {

				if(err) {

					return `Saving Failed`;

				} else {

					return result;
				}
			})
		})

	} else {

		return `Set to Admin Failed`;
	}
}
/*======================[END] SET ADMIN FUNCTIONALITY====================== */




/*====================[SECTION] CREATE ORDER (NON-ADMIN)==================== */
module.exports.createOrder = async (data) => {
    console.log(data)
        if(data.isAdmin === true){
    
             return `Only registered user can order`
    
        } else {
    
            let updatedUser = await User.findById(data.userId).then(user => {
    
                user.products.push({itemId : data.itemId})
                
                return user.save().then((user, err) => {
    
                    if(err){
    
                        return false
    
                    } else {
    
                        return true
                    }
                })
            })
    
            let updatedItem = await paMine.findById(data.itemId).then(Pamine => {
    
                Pamine.mgaPaMine.push({userId : data.userId})
    
                return Pamine.save().then((Pamine, err) => {
    
                    if(err){
    
                         return false
    
                    } else {
    
                        return true
                    }
                })
            })
    
            if(updatedUser && updatedItem){
    
                return `Ordered successfully`
    
            } else {
    
                return `Try again`
            }
        }
    }
/*======================[END] CREATE ORDER (NON-ADMIN)====================== */
    