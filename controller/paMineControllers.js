const paMine = require('../model/PaMine');
const bcrypt = require('bcrypt');
const auth = require('../auth')



/*===========================[SECTION] ADD ITEM=========================== */

module.exports.addItem = async (data) => {
	console.log(data)
		// Access to admin
		if (data.isAdmin) {
	
			
			let newItem = new paMine({
				name : data.item.name,
                size : data.item.size,
                color: data.item.color,
				description : data.item.description,
				price : data.item.price
			});
	
			// Saves the created object to our database
			return newItem.save().then((item, error) => {
	
				// Course creation successful
				if (error) {
	
					return false;
	
				// Course creation failed
				} else {
	
					return `Item added ${item}`;
	
				};
	
			});
	
		// User is not an admin
		} else {
			return `User is not an Admin`;
		};
		
	};

/*===========================[END] ADD ITEM=========================== */


/*====================[SECTION] RETRIEVE ALL ITEMS==================== */

module.exports.allItems = async (user) => {
	
    if(user.isAdmin === false){

        return paMine.find({}).then(result => {

            return result
        })

    } else {

        return `${user.email} is not authorized`
    }
}

/*=======================[END] RETRIEVE ALL ITEMS======================= */



/*====================[SECTION] RETRIEVE SPECIFIC ITEMS==================== */

module.exports.getItem = (requestParams) => {
	
    return paMine.findById(requestParams.itemId).then(result => {

        return result
    })
}
/*======================[END] RETRIEVE SPECIFIC ITEMS====================== */




/*=========================[SECTION] UPDATING ITEM========================= */
module.exports.updateItem = (data) => {
    
    return paMine.findById(data.itemId).then((result,err) => {
        
        if(data.payload.isAdmin === true){
        
                result.name = data.updatedItem.name
                result.size = data.updatedItem.size
                result.color = data.updatedItem.color
                result.description = data.updatedItem.description
                result.price = data.updatedItem.price
    
            return result.save().then((updatedItem, err) => {
                if(err){
                    return `Error in Saving`
                } else {
                    return updatedItem
                }
            })
        } else {
            return `Update failed`
        }
    })
}
/*==========================[END] UPDATING ITEM========================== */





/*=========================[SECTION] AVAILABLE ITEM========================= */
module.exports.allAvailable = () => {
		
    return paMine.find({isAvailable : true}).then(result => {

        return result
    })
}
/*==========================[END] AVAILABLE ITEM========================== */





/*=========================[SECTION] ARCHIVE ITEM======================== */
module.exports.archiveItem = async (data) => {

	if(data.payload.isAdmin === true) {

		return paMine.findById(data.itemId).then((result, err) => {

			result.isAvailable = false;

			return result.save().then((archivedItem, err) => {

				if(err) {

					return false;

				} else {

					return result;
				}
			})
		})

	} else {

		return false;
	}
}
/*========================[END] ARCHIVE ITEM========================== */