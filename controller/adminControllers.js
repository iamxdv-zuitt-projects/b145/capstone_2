
const Admin = require('../model/Admin');
const bcrypt = require('bcrypt');
const auth = require('../auth')


/*======================[SECTION] ADMIN REGISTRATION====================== */

module.exports.registerAdmin = (reqBody) => {

    let newAdmin = new Admin({
        firstName : reqBody.firstName,
        lastName : reqBody.lastName,
        gender : reqBody.gender,
        email : reqBody.email,
        mobileNo : reqBody.mobileNo,
        password : bcrypt.hashSync(reqBody.password, 10)
    })

    return newAdmin.save().then((admin, err) => {

        if(err){

             return `Registration not successful`

        } else {

            return `Registration successful`
        }
    })
}
/*======================[END] ADMIN REGISTRATION====================== */


/*====================[SECTION] USER AUTHENTICATION==================== */
module.exports.loginAdmin = (reqBody) => {

	return Admin.findOne({email : reqBody.email}).then(result => {

		if(result == null){

			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password) 

			if(isPasswordCorrect){

				return { access: auth.createAccessToken(result)}
			} else {

				return false
			}
		}
	})
}

/*======================[END] USER AUTHENTICATION====================== */



/*====================[SECTION] RETRIEVE ALL ADMIN==================== */
module.exports.allAdmin = async (user) => {
	
    if(user.isAdmin === true){

        return Admin.find({}).then(result => {

            return result
        })

    } else {

        return `${user.email} is not ADMIN`
    }
}


/*======================[END] RETRIEVE ALL ADMIN====================== */